#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import time, requests, json
from temp import MyTemp
print("---")
print("Iniciando Sistema de Alertas por Altas Temperaturas")
print("by Marc G. & Matias C.")
print("---")


with open("/home/pi/python/token.json") as file:
    token = json.load(file)["token"]
url = 'https://api.telegram.org/bot'+token+'/'
with open("/home/pi/python/chat_id.json") as file:
    chat_id = json.load(file)["chat_id"]
def Alert():
## Llamamos a la función que lee la temperatura
    while True:
        temp = round(MyTemp.loop())
        if temp >= 30:
            print("Alerta")
            r1 = requests.post(url+'sendMessage', data={"chat_id": chat_id, "text": '¡ALERTA! La temperatura empieza a ser demasiado alta: '+str(temp)+'ºC'})
            time.sleep(30)

Alert()

